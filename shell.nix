with import <nixpkgs> {};

(mkShell.override {stdenv = stdenvNoCC;} ) {
    name = "seeker";
    buildInputs = [
        haskellPackages.stan
        haskellPackages.stylish-haskell
        haskellPackages.hpack
    ];
    shellHook = "
    ";
}
