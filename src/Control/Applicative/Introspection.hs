{-# OPTIONS_HADDOCK show-extensions #-}
{-# LANGUAGE DeriveFunctor      #-}
{-# LANGUAGE DerivingStrategies #-}

{- | A subset of 'Data.Binary.Get' functionality enhanced with static data on number of bytes consumed.
-}

module Control.Applicative.Introspection (GetI, runGetI,
        getWord8,
        getWord16le, getWord16be,
        getWord32le, getWord32be,
        getWord64le,  getWord64be,
        getInt8,
        getInt16le, getInt16be,
        getInt32le, getInt32be,
        getInt64le, getInt64be,
        getByteString, getLazyByteString,
        getFloatle, getFloatbe,
        getDoublele, getDoublebe,
        skip) where

import           Data.Binary          (Get)
import qualified Data.Binary.Get      as BG
import qualified Data.ByteString      as BS
import qualified Data.ByteString.Lazy as BL
import           Data.Coerce          (coerce)
import           Data.Int             (Int16, Int32, Int64, Int8)
import           Data.Monoid          (Sum (Sum))
import           Data.Word            (Word16, Word32, Word64, Word8)


{- | Wrapper around 'Data.Binary.Get' that stores static info on number of consumed bytes

These static guarantees allow for 'Prelude.Functor' and 'Prelude.Applicative' instances, but 'Prelude.Monad' is off the list.

'Control.Applicative.Alternative' does not work either, because its options can consume different number of bytes.

This is an opaque type; we expose only primitive getters with known static guarantees.

@since 0.1.0.0 -}
newtype GetI a = GetI (Sum Int, Get a) deriving stock Functor


instance Applicative GetI where
    pure x = GetI (mempty, pure x)
    GetI (s1, g1) <*> GetI (s2, g2) = GetI (s1 <> s2, g1 <*> g2)

-- | recover the 'Get' action and the number of bytes it would consume
runGetI :: GetI a -> (Int, Get a)
runGetI = coerce


-- | Read a Word8; one byte
getWord8 :: GetI Word8
getWord8 = coerce (1 :: Int, BG.getWord8)

getWord16le, getWord16be :: GetI Word16
-- | Read a Word16 in little endian format; 2 bytes
getWord16le = coerce (2 :: Int, BG.getWord16le)
-- | Read a Word16 in big endian format; 2 bytes
getWord16be = coerce (2 :: Int, BG.getWord16be)

getWord32le, getWord32be :: GetI Word32
-- | Read a Word32 in little endian format; 4 bytes
getWord32le = coerce (4 :: Int, BG.getWord32le)
-- | Read a Word32 in big endian format; 4 bytes
getWord32be = coerce (4 :: Int, BG.getWord32be)

getWord64le, getWord64be :: GetI Word64
-- | Read a Word64 in big endian format; 8 bytes
getWord64le = coerce (8 :: Int, BG.getWord64le)
-- | Read a Word64 in big endian format; 8 bytes
getWord64be = coerce (8 :: Int, BG.getWord64be)

-- | Read an Int8; one byte
getInt8 :: GetI Int8
getInt8 = coerce (1 ::Int, BG.getInt8)

getInt16le, getInt16be :: GetI Int16
-- | Read an Int16 in little endian format; 2 bytes
getInt16le = coerce (2 :: Int, BG.getInt16le)
-- | Read an Int16 in big endian format; 2 bytes
getInt16be = coerce (2 :: Int, BG.getInt16be)

getInt32le, getInt32be :: GetI Int32
-- | Read an Int32 in little endian format; 4 bytes
getInt32le = coerce (4 :: Int, BG.getInt32le)
-- | Read an Int32 in big endian format; 4 bytes
getInt32be = coerce (4 :: Int, BG.getInt32be)

getInt64le, getInt64be :: GetI Int64
-- | Read an Int64 in little endian format; 8 bytes
getInt64le = coerce (8 :: Int, BG.getInt64le)
-- | Read an Int64 in big endian format; 8 bytes
getInt64be = coerce (8 :: Int, BG.getInt64be)

-- | Skip ahead @n@ bytes. See also 'Data.Binary.Get.skip'
skip ::
    Int -- ^ Number of bytes to skip
    -> GetI ()
skip n = coerce (n, BG.skip n)

-- | Get method for lazy ByteStrings. Fails if fewer than n bytes are left in the input.
-- @see 'Data.Binary.Get.getLazyByteString'
getLazyByteString :: Int64 -> GetI BL.ByteString
getLazyByteString n = GetI (Sum $ fromIntegral n, BG.getLazyByteString n)

-- | Get method for strict ByteStrings. Fails if fewer than @n@ bytes are left in the input. If @n <= 0@ then the empty string is returned.
-- @see 'Data.Binary.Get.getLazyByteString'
getByteString :: Int -> GetI BS.ByteString
getByteString n = coerce (n, BG.getByteString n)

-- | Read a 'Float' in big endian IEEE-754 format; 4 bytes
getFloatbe :: GetI Float
getFloatbe = coerce (4 :: Int, BG.getFloatbe)

-- | Read a 'Float' in little endian IEEE-754 format; 4 bytes
getFloatle :: GetI Float
getFloatle = coerce (4 :: Int, BG.getFloatle)

-- | Read a 'Double' in big endian IEEE-754 format; 8 bytes
getDoublebe :: GetI Double
getDoublebe = coerce (8 :: Int, BG.getDoublebe)

-- | Read a 'Double' in little endian IEEE-754 format; 8 bytes
getDoublele :: GetI Double
getDoublele = coerce (8 :: Int, BG.getDoublele)
