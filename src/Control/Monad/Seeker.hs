{-# OPTIONS_HADDOCK show-extensions #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE UndecidableInstances       #-}

{- |

A combination of 'System.IO.hSeek' and 'Data.Binary.Get'.

The 'Data.Binary.Get.skip' can not handle negative moves, and sometimes we need to move in file backwards (for example a file with TOC in its footer, not its header),
hence the need for 'hSeek'. On the other hand, we need to know the number of bytes to be read in advance for 'Data.ByteString.hGet'/'Data.ByteString.hGetSome', hence the need for 'GetI'.

Since we are using 'hSeek', we limit this functionality to files (or, more generally, "seekable" handles).

If we interpret the currect read position as state, we can mimick the same behavior for regular bytestring (see, for example, 'runWithBS')
-}
module Control.Monad.Seeker (
    -- * Typeclass
    MonadSeeker(seek, embed, size),
    -- * Implementation over a file handle
    Seeker (Seeker), runWithSeeker,
    -- * Implementation over a bytestring
    runWithBS, runWithTBS,
    -- * Data types
    Offset(Offset),
    -- * Convenience reexports
    SeekMode (AbsoluteSeek, RelativeSeek, SeekFromEnd)
    ) where

import           Control.Applicative.Introspection (GetI, runGetI)
import           Control.Monad.IO.Class            (MonadIO, liftIO)
import           Control.Monad.Reader              (MonadReader,
                                                    ReaderT (runReaderT))
import qualified Control.Monad.Reader
import           Control.Monad.State               (MonadState (get, put))
import           Control.Monad.Trans.Class         (MonadTrans)
import           Control.Monad.Trans.RWS           (RWS, RWST, evalRWS,
                                                    evalRWST)
import           Control.Monad.Trans.Reader        (asks)
import           Data.Binary.Get                   (runGet)
import qualified Data.ByteString                   as BS
import qualified Data.ByteString.Lazy              as BL
import           Data.Int                          (Int64)
import           System.IO                         (Handle, IOMode (ReadMode),
                                                    SeekMode (AbsoluteSeek, RelativeSeek, SeekFromEnd),
                                                    hFileSize, hSeek, withFile)


-- | Wrapper to represent offset in the file.
-- Negative values are requried for 'SeekFromEnd' and negative 'RelativeSeek'
newtype Offset = Offset Int64

-- | Class to combine the functionality of 'hSeek' and 'Get'
class Monad m => MonadSeeker m where
    seek :: SeekMode -> Offset -> m () -- ^ move the "pointer". See also 'System.IO.hSeek'.
    embed :: GetI a -> m a -- ^ run a 'GetI' action. It should move the current 'Offset' by the number of bytes specified by that action.
    size :: m Int  -- ^  file size helper


-- | Overlappable instance of 'MonadSeeker'
newtype Seeker m a = Seeker {runSeeker :: ReaderT (Handle, Int) m a} deriving newtype (Functor, Applicative, Monad, MonadIO, MonadFail, MonadTrans)
-- | Overlapping instance for 'Seeker'
instance {-# OVERLAPPING #-} MonadIO m => MonadSeeker (Seeker m) where
    size = Seeker $ asks snd

    seek mode (Offset offset) = Seeker $ do
        h <- asks fst
        liftIO $ hSeek h mode (fromIntegral offset)

    -- | note that this implementation moves the position of the underlying 'Handle' by @n@ bytes.
    embed act = let (n, g) = runGetI act in Seeker $ do
        h <- asks fst
        bs <- liftIO $ BS.hGetSome h n
        let val = runGet g $ BL.fromStrict bs
        pure val

-- | Runner for 'MonadSeeker' via 'Seeker'.
-- 'FilePath' is opened in 'ReadMode'.
runWithSeeker :: FilePath -> Seeker IO a -> IO a
runWithSeeker path impl = withFile path ReadMode runner
  where
    runner h = do
        l <- (fromIntegral <$> hFileSize h) :: IO Int
        runReaderT (runSeeker impl) (h, l)

-- | Runner for 'MonadReader'/'MonadState'-based instance via 'RWS'
runWithBS :: BL.ByteString -> RWS BL.ByteString () Offset a -> a
runWithBS bs comp = fst $ evalRWS comp bs (Offset 0)

-- | Runner for 'MonadReader'/'MonadState'-based instance via 'RWST'
runWithTBS :: Monad m => BL.ByteString -> RWST BL.ByteString () Offset m a -> m a
runWithTBS bs comp = fst <$> evalRWST comp bs (Offset 0)

-- | Base instance in terms of mtl classes for in-RAM bytestrings
instance {-# OVERLAPPABLE #-}  (Monad m, MonadReader BL.ByteString m, MonadState Offset m) => MonadSeeker m where
    size = Control.Monad.Reader.asks (fromIntegral . BL.length)

    seek AbsoluteSeek o = put o
    seek RelativeSeek (Offset delta) = do
        Offset current <- get
        put $ Offset $ current + delta
    seek SeekFromEnd  (Offset delta) = do
        s <- size
        put $ Offset $ fromIntegral s + delta

    embed g = let (n, act) = runGetI g in do
        Offset pos <- get
        put $ Offset $ pos + fromIntegral n
        Control.Monad.Reader.asks (runGet act . BL.take (fromIntegral n) . BL.drop pos)
