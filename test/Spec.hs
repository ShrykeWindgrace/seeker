{-# LANGUAGE RankNTypes #-}
import           Control.Applicative.Introspection (getWord16le, getWord32le,
                                                    getWord8)
import           Control.Monad                     (replicateM)
import           Control.Monad.Seeker              (MonadSeeker (embed, seek),
                                                    Offset (Offset), runWithBS,
                                                    runWithSeeker)
import           Data.Binary.Put                   (putWord32le, runPut)
import qualified Data.ByteString.Lazy              as BL
import           Data.Char                         (ord)
import           Data.Word                         (Word8)
import           System.IO.Extra                   (SeekMode (AbsoluteSeek, RelativeSeek, SeekFromEnd),
                                                    withTempFile)
import           Test.Hspec                        (Spec, describe, hspec, it,
                                                    shouldBe, shouldReturn)


import qualified Example

main :: IO ()
main = do
    withTempFile $ \fp -> do
        let s = "123"
        writeFile fp s
        let codes = fromIntegral . ord <$> s
        hspec $ do
            describe "reading with absolute offset" $ do
                it "direct" $ do
                    runWithSeeker fp direct `shouldReturn` codes
                it "reverse" $ do
                    runWithSeeker fp reverseThree `shouldReturn` reverse codes
    withTempFile $ \fp -> do
        let bs = runPut $ putWord32le 226
        BL.writeFile fp bs
        hspec $ do
            describe "pure runner" $ g (pure . runWithBS bs)
            describe "with file" $ g (runWithSeeker fp)

    Example.spec

reverseThree :: (MonadSeeker m) => m [Word8]
reverseThree = do
    seek AbsoluteSeek $ Offset 2
    x2 <- embed getWord8
    seek AbsoluteSeek $ Offset 1
    x1<- embed getWord8
    seek AbsoluteSeek $ Offset 0
    x0 <- embed getWord8
    pure [x2, x1, x0]

direct :: MonadSeeker m => m [Word8]
direct = do
    seek AbsoluteSeek $ Offset 0
    embed (replicateM 3 getWord8)

-- |  Accepts both hSeek-based and pure 'Seeker' runners
g :: MonadSeeker m => (forall a.(Show a, Eq a) => m a -> IO a) -> Spec
g runner = describe "4 bytes" $ do
    it "word 32 le" $
        runner (seek AbsoluteSeek (Offset 0) >> embed getWord32le) `shouldReturn` 226
    it "byte per byte" $ do
        runner (seek AbsoluteSeek (Offset 0) >> embed getWord8) `shouldReturn` 226
        runner (seek AbsoluteSeek (Offset 1) >> embed getWord8) `shouldReturn` 0
    it "repeated reads" $ do
        runner ((,) <$> embed getWord8 <*> (seek RelativeSeek (Offset $ -1) *> embed getWord8)) `shouldReturn` (226, 226)
    it "seek from end" $
        runner (seek SeekFromEnd  (Offset $ -2) >> embed getWord16le) `shouldReturn` 0
