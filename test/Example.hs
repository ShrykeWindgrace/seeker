{- |
Suppose that for some reasons we encode a list of 'Word64' as follows:

- write these integers as little-endian Word8/Word16/Word32/Word64 depending on their value

- then write list of number of bytes taken by each respective integer as Word8

- finally, write a total count of input integers as little-endian Word32

For example, writing a list @[1, 1024] :: [Word64]@ is equivalent to writing

@[1:: Word8, 1024::Word16le, 1::Word8, 2::Word8, 2::Word32le]@

So instead of writing 16 bytes we wrote only 10. However, in order to decode these integers:

- we need to first read the count
- then go back to determine the bytelength of these integers
- then go back again to the begging of the file to finally be able to read the desired list

Our 'MonadSeeker' simplifies the decoding of such files
-}
module Example (spec) where

import           Data.Binary                       (Put)
import           Data.Binary.Put                   (putWord16le, putWord32le,
                                                    putWord64le, putWord8,
                                                    runPut)
import qualified Data.ByteString.Lazy              as BL
import           Data.Monoid                       (Sum (Sum, getSum))
import           Data.Word                         (Word16, Word32, Word64,
                                                    Word8)
import           System.IO.Extra                   (withTempFile)
import           Test.Hspec                        (describe, hspec, it,
                                                    shouldBe, shouldReturn)

import           Control.Applicative.Introspection (GetI, getWord16le,
                                                    getWord32le, getWord64le,
                                                    getWord8)
import           Control.Monad                     (replicateM)
import           Control.Monad.Seeker              (MonadSeeker (embed, seek),
                                                    Offset (Offset),
                                                    SeekMode (AbsoluteSeek, SeekFromEnd),
                                                    Seeker, runWithBS,
                                                    runWithSeeker)


-- | Showcase that encoding and decoding work
spec :: IO ()
spec = withTempFile $ \fp -> do
        let list = [0, 255, 1024, maxBound :: Word64, fromIntegral (maxBound:: Word32) - 1]
        let bs = encode list
        BL.writeFile fp bs
        hspec $
            describe "~varint~ decoding" $ do
                it "file: should return the same list" $
                    runWithSeeker fp decode `shouldReturn` list
                it "RAM: should return the same list" $
                    runWithBS bs decode `shouldBe` list

-- | encode integers
encode :: [Word64] -> BL.ByteString
encode ints = let
    (datas, tokens, l) = foldMap split ints
    act = datas <> tokens <> putWord32le (getSum l)
    in runPut act

-- | action to write the integer using reduced number of bytes and then to write that number
split :: Word64 -> (Put, Put, Sum Word32)
split n
    | n <= fromIntegral (maxBound :: Word8) = (putWord8 $ fromIntegral n, putWord8 1, Sum 1)
    | n <= fromIntegral (maxBound :: Word16) = (putWord16le $ fromIntegral n, putWord8 2, Sum 1)
    | n <= fromIntegral (maxBound :: Word32) = (putWord32le $ fromIntegral n, putWord8 4, Sum 1)
    | otherwise = (putWord64le n, putWord8 8, Sum 1)

decode :: MonadSeeker m => m [Word64]
decode = do
    seek SeekFromEnd $ Offset (-4)
    listLength <- embed getWord32le
    seek SeekFromEnd $  Offset (-4 - fromIntegral listLength)
    tokens <- embed $ replicateM (fromIntegral listLength) getWord8
    seek AbsoluteSeek $ Offset 0
    embed $ traverse glue tokens

glue :: Word8 -> GetI Word64
glue 1 = fromIntegral <$> getWord8
glue 2 = fromIntegral <$> getWord16le
glue 4 = fromIntegral <$> getWord32le
glue 8 = fromIntegral <$> getWord64le
glue _ = error "wrong byte length"
