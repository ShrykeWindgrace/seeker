{
  description = "seeker";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs =
    { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      # system = "x86_64-linux";
      ghc = "ghc98";
      pkgs = nixpkgs.legacyPackages.${system};

      haskellPackages = pkgs.haskell.packages.${ghc};
      # pkgs.haskell.packages.${ghc}.extend(hself: hsuper: {
      # seeker = haskellPackages.callCabal2nix "seeker" "${self}/" {};
      # });
      packageName = "seeker";
    in
    {
      packages.${packageName} =
        haskellPackages.callCabal2nix packageName self rec {
          # Dependency overrides go here
        };
      packages.default = self.packages.${system}.${packageName};
      devShells.default = pkgs.mkShell {
        buildInputs = [
          # (haskellPackages.ghcWithPackages(p: p.seeker.getCabalDeps.executableHaskellDepends))
          # haskellPackages.seeker.getCabalDeps.executableToolDepends
          # haskellPackages.cabal-install
        ];
        inputsFrom = builtins.attrValues self.packages.${system};
      };
    });
}
