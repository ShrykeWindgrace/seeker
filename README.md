# seeker

Combine the power of `Get` monad with explicit pointer placement by `hSeek`

## Motivation

The `Get` monad does not allow for negative skips - if the file format requires reading the tail of the file to decode its head
(for example, if TOC is appeneded, not prepended), we are bound to do a lot juggling with the bytestring representing this file.

## Code example

(Directly extracted from `test/Example.hs`)

Suppose that for some reason we encode a list of `Word64` as follows:

- write these integers as little-endian Word8/Word16/Word32/Word64 depending on their value

- then write list of number of bytes taken by each respective integer as Word8

- finally, write a total count of input integers as little-endian Word32

For example, writing a list `[1, 1024] :: [Word64]` is equivalent to writing

`[1:: Word8, 1024::Word16le, 1::Word8, 2::Word8, 2::Word32le]`

So instead of writing 16 bytes we wrote only 10. However, in order to decode these integers:

- we need to first read the count
- then go back to determine the bytelength of these integers
- then go back again to the begging of the file to finally be able to read the desired list

Our `MonadSeeker` simplifies the decoding of such files

```haskell

decode :: MonadSeeker m => m [Word64]
decode = do
    seek SeekFromEnd $ Offset (-4)
    listLength <- embed getWord32le
    seek SeekFromEnd $  Offset (-4 - fromIntegral listLength)
    tokens <- embed $ replicateM (fromIntegral listLength) getWord8
    seek AbsoluteSeek $ Offset 0
    embed $ traverse glue tokens

glue :: Word8 -> GetI Word64
glue 1 = fromIntegral <$> getWord8
glue 2 = fromIntegral <$> getWord16le
glue 4 = fromIntegral <$> getWord32le
glue 8 = fromIntegral <$> getWord64le

```

## Rendered haddock documentation

https://shrykewindgrace.gitlab.io/seeker/